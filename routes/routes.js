//routes - contains all endpoints for the app; instead of putting a lot of routes in the index.js, we separate them in other file called routes.js (databaseRoutes.js i.e. taskRoutes.js)

const express = require("express")

//creates router instance that functions as a middleware & routing system; allows access to HTTP methods middlewares that makes it easier to create routes for our app
const router = express.Router();

//allows us to use the controllers' functions
const taskController = require("../controllers/controllers.js")

//before, "/" means accessing localhost:3000/
//through use of app.use("/tasks", taskRoute) in index.js for taskRoutes, localhost:3000/tasks/
//accessing different schema (assiming there are more) would setup another app.use in index.js & other files inside models, routes & controllers folder
//example, if you add anpother schema that is Users, the base uri would be set by app.use("/users", userRoute) & accessing its "/" url inside userRoutes would be localhost:3000/users/ 
//route to get all tasks
router.get("/", (req, res)=> {
	//taskController - controller file that's existing in the repo
	//getAllTasks - function encoded inside the taskController file
	//use .then to wait for the .getalltasks function to be executed before proceeding to the statements (res.send(result))
	taskController.getAllTasks().then(result => res.send(result));
})

//route for creating a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

/*
	: - wildcard; if tagged w parameter, the app will look or parameter that's in the url
*/
//localhost:3000/tasks/6267dcb26c34969701eeebf9
router.delete("/:id", (req, res) => {
	//URL parameter values are accessed through the request obj's "params" property; the specified parameter/property of the obj will match the URL parameter endpoint
	taskController.deleteTask(req.params.id).then(result => res.send(result))
})


router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

router.get("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

router.put("/:id/complete", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})








//module.exports - a way for JS to treat value/file as a pkg that can be imported/used by other files
module.exports = router;//always at the last line of file