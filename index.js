const express = require("express");
const mongoose = require("mongoose");

//to load the pkg inside the routes.js file in the repo
const taskRoute = require("./routes/routes.js")

const app = express();
const port = 3000;

//gives app access to routes needed
//will set...
app.use(express.json());//lets app read json data
app.use(express.urlencoded({extended: true})); //allows app to receive data from forms


mongoose.connect("mongodb+srv://clarissacristobal:clarissacristobal@wdc028-course-booking.s6ug6.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
})

let db = mongoose.connection;
// if an error existed in connecting to MongoDB
db.on("error", console.error.bind(console, "Connection Error"));
// if the connection is successful
db.once("open", () => console.log("We're connected to the database"))

//gives app access to routes needed
app.use("/tasks", taskRoute)









app.listen(port, () => console.log(`Server is running at port ${port}`)); //must be only 1
